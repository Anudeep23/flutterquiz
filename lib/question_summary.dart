import 'package:flutter/material.dart';
import 'package:quiz_app/models/summary_item.dart';

class QuestionSummary extends StatelessWidget
{
  const QuestionSummary(this.SummaryData , {super.key});
  final List <Map<String ,Object>> SummaryData;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: SingleChildScrollView(
        child: Column(
          children: SummaryData.map((data){
              return SummaryItem(data);
          },
          ).toList(),
        ),
      ),
    );
  }
}