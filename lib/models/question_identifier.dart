import 'package:flutter/material.dart';

class QuestionIdentifier extends StatelessWidget
{
  const QuestionIdentifier(
    {
      super.key,
      required this.isCorrectAns,
      required this.questionIndex
    }
  );
  final int questionIndex;
  final bool isCorrectAns;
  
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    final questionNumber = questionIndex+1;
    return Container(
      width:30,
      height:30,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: isCorrectAns? Colors.green : Colors.red,
        borderRadius: BorderRadius.circular(100),
      ),

      child: Text(questionNumber.toString() , 
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black
      ),),
    );
  }
}